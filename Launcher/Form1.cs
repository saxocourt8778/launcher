﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Launcher
{
    public partial class Form1 : Form
    {
        // Keep track of the update thread, so we can terminate it when form closes.
        private Thread tUpdateThread;


        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e) {
            // Display the OFD and see what the result is.
            DialogResult drOpenResult = ofdProgram.ShowDialog();
            if(drOpenResult == DialogResult.OK) {
                // File chosen. Copy path into text box, then enable the Start button.
                txtProgram.Text = ofdProgram.FileName;
                btnStart.Enabled = true;
            }
        }

        private void btnStart_Click(object sender, EventArgs e) {
            // Create Process object to run the chosen program.
            Process prNewProgram = new Process();
            // Set path to program to execute.
            prNewProgram.StartInfo.FileName = txtProgram.Text;
            // Start the process.
            prNewProgram.Start();
            // Display information about the process in the DGV.
            object[] objProcessInfo = new object[4];
            objProcessInfo[0] = prNewProgram.ProcessName + " - " + Path.GetFileName(txtProgram.Text);
            objProcessInfo[1] = prNewProgram.Id;
            objProcessInfo[2] = prNewProgram.TotalProcessorTime;
            objProcessInfo[3] = prNewProgram;
            dgvProcesses.Rows.Add(objProcessInfo);
        }

        private void btnTerminate_Click(object sender, EventArgs e) {
            // Get the selected process out of the DGV.
            DataGridViewRow dgvrSelectedProc = dgvProcesses.SelectedRows[0];
            Process prToKill = (Process)(dgvrSelectedProc.Cells[3].Value);
            // Check whether it has already exited. If not, kill it.
            if (!prToKill.HasExited) {
                prToKill.Kill();
            }
            // Clean up the information about the process.
            prToKill.Close();
            // Remove the process' info from the DGV.
            dgvProcesses.Rows.Remove(dgvrSelectedProc);
        }

        // Function that periodically refreshes the info in the DGV
        private void vUpdateDGV() {
            // Loop forever.
            while (true) {
                // Wait 500 milliseconds.
                Thread.Sleep(500);
                // Loop through the rows in the DGV
                for (int iRow = 0; iRow < dgvProcesses.Rows.Count; iRow++) {
                    // Get the current row.
                    DataGridViewRow dgvrCurrent = dgvProcesses.Rows[iRow];
                    // Get its process.
                    Process prCurrent = (Process)(dgvrCurrent.Cells[3].Value);
                    // Refresh the info on the process.
                    prCurrent.Refresh();
                    // Check whether the process has exited. If so, remove it. 
                    // If not, update the CPU time.
                    if (prCurrent.HasExited) {
                        // Remove from DGV.
                        // Create delegate packaging information about this function.
                        dRemoveDel drThisFcn = new dRemoveDel(vRemoveDGVRow);
                        // Create an array to hold the parameter to pass to this function 
                        // when it is invoked (row number)
                        object[] objParam = new object[1];
                        objParam[0] = iRow;
                        // Tell the DGV to execute this function using the proper thread.
                        dgvProcesses.Invoke(drThisFcn, objParam);
                        // Decrement row number, so we don't skip a row.
                        iRow--;
                    } else {
                        dgvrCurrent.Cells[2].Value = prCurrent.TotalProcessorTime;
                    }
                }
            }
        }

        // Delegate for a function with one int parameter and void return type.
        private delegate void dRemoveDel(int iRowNum);

        // Function to remove a row from the DGV in a way that is thread-safe.
        private void vRemoveDGVRow(int iRow) {
            dgvProcesses.Rows.RemoveAt(iRow);
        }

        private void Form1_Load(object sender, EventArgs e) {
            // Set up and start a thread to do the periodic update of the DGV.
            // Create delegate telling what function to execute.
            ThreadStart tsUpdateFunction = new ThreadStart(vUpdateDGV);
            // Create thread.
            tUpdateThread = new Thread(tsUpdateFunction);
            // Start thread.
            tUpdateThread.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            tUpdateThread.Abort();
        }
    }
}
